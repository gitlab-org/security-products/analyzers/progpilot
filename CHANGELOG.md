# Progpilot analyzer changelog

GitLab Progpilot analyzer follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/analyzers/progpilot/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.

## 11-0-stable

## 10-8-stable
- initial release
