package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const (
	wrapperDir      = "/progpilot"
	wrapperFilename = "wrapper.php"

	pathPHP    = "/usr/local/bin/php"
	pathInput  = "/tmp/progpilot.files"
	pathOutput = "/tmp/progpilot.json"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// list PHP files into a file
	f, err := os.OpenFile(pathInput, os.O_WRONLY|os.O_CREATE, 6440)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	if err := filepath.Walk(path, func(fpath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			if info.Name() == "test" {
				// Skip test directory
				return filepath.SkipDir
			}
		} else {
			if filepath.Ext(info.Name()) == ".php" {
				fmt.Fprintln(f, fpath)
			}
		}
		return nil
	}); err != nil {
		return nil, err
	}
	f.Close()

	// NOTE: Wrapper must run in its own directory because
	// it uses relative paths to include vendor libraries.
	cmd := exec.Command(pathPHP, wrapperFilename, pathInput, pathOutput)
	cmd.Dir = wrapperDir
	cmd.Env = os.Environ()
	cmd.Stdout = c.App.Writer
	cmd.Stderr = c.App.Writer // DEBUG
	if err := cmd.Run(); err != nil {
		return nil, err
	}

	return os.Open(pathOutput)
}
