package main

import (
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

func init() {
	os.Setenv("CI_PROJECT_DIR", "/tmp/project")
}

func TestConvert(t *testing.T) {
	in := `[
   {
      "source_column" : [
         27
      ],
      "vuln_name" : "xss",
      "source_name" : [
         "$var4"
      ],
      "sink_name" : "echo",
      "sink_column" : 47,
      "sink_file" : "/tmp/project/progpilot/test/test.php",
      "source_file" : [
         "/tmp/project/progpilot/test/test.php"
      ],
      "sink_line" : 5,
      "vuln_cwe" : "CWE_79",
      "vuln_id" : "f425da3611c731ac8ca6e9f59d7eb4e1f0838e0de733d04bffccb8c10dbb697d",
      "source_line" : [
         4
      ]
   },
   {
      "vuln_name" : "file_disclosure",
      "source_name" : [
         "$pattern"
      ],
      "source_column" : [
         329
      ],
      "sink_column" : 393,
      "source_file" : [
         "/tmp/project/progpilot/wrapper.php"
      ],
      "sink_file" : "/tmp/project/progpilot/wrapper.php",
      "sink_line" : 23,
      "sink_name" : "glob",
      "vuln_cwe" : "CWE_200",
      "vuln_id" : "03c3882bff02f064e7147e04ed84b6b071bd30fc26a204092b8bed14b044ab66",
      "source_line" : [
         21
      ]
   }
]`
	r := strings.NewReader(in)
	want := []issue.Issue{
		{
			Tool:       "progpilot",
			Message:    "Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting')",
			URL:        "https://cwe.mitre.org/data/definitions/79.html",
			CompareKey: "app/progpilot/test/test.php:$var4:xss",
			File:       "app/progpilot/test/test.php",
			Line:       5,
			Priority:   "High",
		},
		{
			Tool:       "progpilot",
			Message:    "Information exposure",
			URL:        "https://cwe.mitre.org/data/definitions/200.html",
			CompareKey: "app/progpilot/wrapper.php:$pattern:file_disclosure",
			File:       "app/progpilot/wrapper.php",
			Line:       23,
			Priority:   "High",
		},
	}
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
