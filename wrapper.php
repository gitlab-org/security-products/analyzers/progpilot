<?php

// Check arguments
if ($argc != 3) {
	fwrite(STDERR, <<<EOD
Invalid arguments.
Usage: ./wrapper.php <file> <output>
Check all PHP files listed in given file
and write the result as a JSON document in output file.

EOD
);
	exit(2); 
}

// Initialize analyzer
require_once './vendor/autoload.php';
$analyzer = new \progpilot\Analyzer;

// Analyze given directory
$input = $argv[1];
$fp = fopen($input, 'r');
$results = array();
while (($line = fgets($fp)) !== false) {
	$path = trim($line);
	fwrite(STDOUT, $path); // Show progress
	$context = new \progpilot\Context;
	$context->inputs->set_file($path);
	$analyzer->run($context);
	$r = $context->outputs->get_results();
	if (count($r) > 0) {
		fwrite(STDOUT, " FOUND ".count($r)."\n"); // Show progress
	} else {
		fwrite(STDOUT, " OK\n"); // Show progress
	}
	$results = array_merge($results, $r);
}
fclose($fp);

// Write output file
$output = $argv[2];
$fp = fopen($output, 'w');
fwrite($fp, json_encode($results));
fclose($fp);
?>
