package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

const toolID = "progpilot"

const envVarProjectDir = "CI_PROJECT_DIR"

// ErrCWENotFound is raised when not CWE entry for a given CWE ID.
type ErrCWENotFound struct {
	ID int
}

func (e ErrCWENotFound) Error() string {
	return fmt.Sprintf("CWE entry not found: %d", e.ID)
}

// ErrInvalidCWERef is raised when a reference to a CWE is not like "CWE_123".
type ErrInvalidCWERef struct {
	Ref string
}

func (e ErrInvalidCWERef) Error() string {
	return fmt.Sprintf("Invalid CWE reference: %s", e.Ref)
}

func convert(reader io.Reader, prependPath string) ([]issue.Issue, error) {
	results := []Result{}
	err := json.NewDecoder(reader).Decode(&results)
	if err != nil {
		return nil, err
	}

	issues := make([]issue.Issue, len(results))
	for i, raw := range results {
		r := RelResult{raw, prependPath}
		cwe, err := r.CWE()
		if err != nil {
			return nil, err
		}
		issues[i] = issue.Issue{
			Tool:       toolID,
			File:       r.Filepath(),
			Message:    cwe.Title,
			URL:        cwe.URL(),
			Priority:   cwe.Priority(),
			Line:       r.SinkLine,
			CompareKey: r.CompareKey(),
		}
	}
	return issues, nil
}

// RelResult describes a result with a relative path.
type RelResult struct {
	Result
	PrependPath string
}

// Filepath returns the relative path of the affected file.
func (r RelResult) Filepath() string {
	// HACK: projectDir is the directory where the project is mounted.
	// It's needed when converting the output of progpilot to a SAST report
	// because progpilot generates absolute paths whereas SAST reports
	// should only contain relative paths.
	projectDir := os.Getenv(envVarProjectDir)
	rel, _ := filepath.Rel(projectDir, r.SinkFile)
	return filepath.Join(r.PrependPath, rel)
}

// CompareKey returns a string used to establish whether two issues are the same.
func (r RelResult) CompareKey() string {
	code := r.SourceName[0] // TODO: calculate checksum
	return strings.Join([]string{r.Filepath(), code, r.VulnName}, ":")
}

// Result describes a vulnerability found in the source code.
type Result struct {
	SourceColumn []int    `json:"source_column"`
	VulnName     string   `json:"vuln_name"`
	SourceName   []string `json:"source_name"`
	SinkName     string   `json:"sink_name"`
	SinkColumn   int      `json:"sink_column"`
	SinkFile     string   `json:"sink_file"`
	SourceFile   []string `json:"source_file"`
	SinkLine     int      `json:"sink_line"`
	VulnCwe      string   `json:"vuln_cwe"`
	VulnID       string   `json:"vuln_id"`
	SourceLine   []int    `json:"source_line"`
}

// CWE returns the correspending entry of the CWE database.
func (r Result) CWE() (*CWE, error) {
	parts := strings.SplitN(r.VulnCwe, "_", 2)
	if len(parts) < 2 {
		return nil, ErrInvalidCWERef{r.VulnCwe}
	}
	cweID, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, err
	}
	for _, cwe := range cwes {
		if cwe.ID == cweID {
			return &cwe, nil
		}
	}
	return nil, ErrCWENotFound{cweID}
}

// CWE describes an entry of the CWE database.
type CWE struct {
	ID         int
	Title      string
	Confidence string // Confidence is the likelihood of exploit
}

// URL returns the URL of the page for this particular CWE on cwe.mitre.org.
func (cwe CWE) URL() string {
	return fmt.Sprintf("https://cwe.mitre.org/data/definitions/%d.html", cwe.ID)
}

// Priority returns the normalized priority.
func (cwe CWE) Priority() issue.Priority {
	switch cwe.Confidence {
	case "high":
		return issue.PriorityHigh
	case "medium":
		return issue.PriorityMedium
	case "low":
		return issue.PriorityLow
	default:
		return issue.PriorityUnknown
	}
}

var cwes = []CWE{
	{
		ID:         200,
		Title:      "Information exposure",
		Confidence: "high",
	},
	{
		ID:    384,
		Title: "Session Fixation",
		// Confidence is unknown
	},
	{
		ID:         601,
		Title:      "URL Redirection to Untrusted Site ('Open Redirect')",
		Confidence: "low",
	},
	{
		ID:         78,
		Title:      "Improper Neutralization of Special Elements used in an OS Command ('OS Command Injection')",
		Confidence: "high",
	},
	{
		ID:         79,
		Title:      "Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting')",
		Confidence: "high",
	},
	{
		ID:         862,
		Title:      "Missing Authorization",
		Confidence: "high",
	},
	{
		ID:         89,
		Title:      "Improper Neutralization of Special Elements used in an SQL Command ('SQL Injection')",
		Confidence: "high",
	},
	{
		ID:    90,
		Title: "Improper Neutralization of Special Elements used in an LDAP Query ('LDAP Injection')",
		// Confidence is unkown
	},
	{
		ID:    91,
		Title: "XML Injection (aka Blind XPath Injection)",
		// Confidence is unkown
	},
	{
		ID:         95,
		Title:      "Improper Neutralization of Directives in Dynamically Evaluated Code ('Eval Injection')",
		Confidence: "medium",
	},
	{
		ID:         98,
		Title:      "Improper Control of Filename for Include/Require Statement in PHP Program ('PHP Remote File Inclusion')",
		Confidence: "high",
	},
}
